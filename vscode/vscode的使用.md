# VSCode的使用

本篇文章主要讲的啥？常用快捷键、好用的插件和开发常用功能。如：快速打开文件、切换主题、进入禅模式、多行编辑、列选择、向上/向下复制行、代码块上下移动、代码折叠、代码格式化、Markdown预览、导航到指定行、SVG图片预览、为编辑器设置背景图片、编辑器内实时预览、VSCode同步设置、多个项目在同一窗口打开、代码片段、Emmet等等。

废话不多说，请尽量通读全篇，你定会有所收获。本篇文章会持续更新，欢迎点赞和收藏！

# 【1】键盘快捷键

## 命令面板

根据您当前的上下文访问所有可用命令。

键盘快捷键：<font color=#0072be>Ctrl+Shift+P</font>

![Ctrl+Shift+P](./images/OpenCommandPalatte.gif)

## 快速打开文件

快速打开文件，下拉框展示最近打开的文件。

键盘快捷键：<font color=#0072be>Ctrl+P</font>

![Ctrl+P](./images/QuickOpen.gif)

## 打开最近的文件夹

键盘快捷键：<font color=#0072be>Ctrl+R</font>

显示快速选择下拉列表，其中包含文件>打开最近的列表，其中包含最近打开的文件夹和工作区。点击选中默认在当前窗口打开，按Ctrl 键选择在新窗口打开。

## 改变主题

键盘快捷键：<font color=#0072be>Ctrl+K Ctrl+T</font>

![Ctrl+K Ctrl+T](./images/PreviewThemes.gif)

此外，还可以安装和更改文件图标主题。

![Ctrl+K Ctrl+T](./images/PreviewFileIconThemes.gif)

## 查看或修改键盘快捷键

键盘快捷键：<font color=#0072be>Ctrl+K Ctrl+S</font>，或者 “帮助” => “键盘快捷键参考”。

![Ctrl+K Ctrl+S](./images/keyboard-shortcuts.png)

## 切换命令行面板

键盘快捷键：<font color=#0072be>Ctrl+J</font>

集成终端展示隐藏：键盘快捷键：<font color=#0072be>Ctrl+`</font>

![Ctrl+J](./images/integrated_terminal.png)

## 切换侧边栏

键盘快捷键：<font color=#0072be>Ctrl+B</font>

![Ctrl+B](./images/toggle_side_bar.gif)

## 无干扰禅模式

键盘快捷键：<font color=#0072be>Ctrl+KZ</font>

按两次Esc键可退出禅模式。

![Ctrl+KZ](./images/zen_mode.gif)

## 创建或打开文件

键盘快捷键：<font color=#0072be>Ctrl+单击</font>

可以通过将光标移动到文件链接并使用 Ctrl+单击 来快速打开文件或图像或创建新文件。

![Ctrl+单击](./images/create_open_file.gif)

## 导航历史

键盘快捷键：<font color=#0072be>Ctrl+Tab</font>

![Ctrl+Tab](./images/navigate_history.gif)

## 多光标选择

- 要在任意位置添加光标，请使用鼠标选择一个位置并使用<font color=#0072be>Alt+单击</font>（在 macOS 上为<font color=#0072be>Option+单击</font>）。
- 要在当前位置上方或下方设置光标，请使用：键盘快捷键：<font color=#0072be>Ctrl+Alt+Up</font> 或 <font color=#0072be>Ctrl+Alt+Down</font>。
- 使用快捷键<font color=#0072be>Ctrl+Shift+L</font>，可以将其他光标添加到当前选择的所有匹配项。

![Ctrl+Tab](./images/multicursor.gif)

![Ctrl+Tab](./images/add_cursor_current_selection.gif)

## 列（框）选择

可以在拖动鼠标时按住<font color=#0072be>Shift+Alt</font>（在 macOS 上为<font color=#0072be>Shift+Option</font>）来选择文本块。将在每个选定行的末尾添加一个单独的光标。

![Shift+Alt](./images/column-select.gif)

## 向上/向下复制行

键盘快捷键：<font color=#0072be>Shift+Alt+Up</font> 或 <font color=#0072be>Shift+Alt+Down</font>

![Shift+Alt+Up](./images/copy_line_down.gif)

## 代码块上下移动

可以将若干行选中的代码上下移动。键盘快捷键：<font color=#0072be>Alt+Up</font> 或 <font color=#0072be>Alt+Down</font>

![Alt+Up](./images/move_line.gif)

## 缩小/扩大选择

键盘快捷键：<font color=#0072be>Shift+Alt+Left</font> 或 <font color=#0072be>Shift+Alt+Right</font>

![Shift+Alt+Left](./images/shrink_expand_selection.gif)

## 代码折叠

- 逐层折叠或打开代码，键盘快捷键：<font color=#0072be>Ctrl+Shift+[</font> 或 <font color=#0072be>Ctrl+Shift+]</font>
- 可以使用全部折叠( <font color=#0072be>Ctrl+K Ctrl+0</font> ) 和全部展开( <font color=#0072be>Ctrl+K Ctrl+J</font> )折叠/展开编辑器中的所有区域。
- 可以使用折叠所有块注释( Ctrl+K Ctrl+/ )折叠所有块注释。

![Ctrl+Shift+](./images/code_folding.gif)

## 去除尾部空格

去除选定代码块尾部的空格，可以是若干行。键盘快捷键：<font color=#0072be>Ctrl+K Ctrl+X</font>

![Ctrl+K Ctrl+X](./images/trim_whitespace.gif)

## 转换文本命令

可以使用命令面板中的转换命令将选定文本更改为大写、小写和标题大小写。这个功能不是通过快捷键实现。

![Ctrl+K Ctrl+X](./images/transform-text-commands.png)

## 代码格式化

- 当前选中的源代码：<font color=#0072be>Ctrl+K Ctrl+F</font>
- 整个文档格式：<font color=#0072be>Shift+Alt+F</font>

![Ctrl+K Ctrl+F](./images/code_formatting.gif)

## 打开 Markdown 预览

在 Markdown 文件中，使用键盘快捷键：<font color=#0072be>Ctrl+Shift+V</font>

![Ctrl+Shift+V](./images/markdown-preview.png)

## 并排 Markdown 编辑和预览

在 Markdown 文件中，使用键盘快捷键：<font color=#0072be>Ctrl+KV</font>。预览和编辑器将与您在任一视图中的滚动同步。

![Ctrl+KV](./images/markdown-preview-side-by-side.png)

## 转到定义

选择一个符号，然后输入F12。或者，您可以使用上下文菜单或 <font color=#0072be>Ctrl+单击</font>（在 macOS 上为<font color=#0072be>Cmd+单击</font>）。

![Cmd+单击](./images/goto_definition.gif)

其他常用快捷键：
- 撤销（Ctrl+Z）
- 选择当前行（Ctrl+L）
- 导航到特定行（Ctrl+G）
- 导航到文件的开头 （Ctrl+Home）
- 导航到文件的结尾 （Ctrl+End）
- 打开新窗口 （Ctrl+Shift+N）
- 打开文件夹 （Ctrl+K Ctrl+O）
- 在文件资源管理器中显示（Shift+Alt+R）
- 复制路径（Shift+Alt+C）

# 【2】插件

对于插件的安装，不要人云亦云，要看自己喜不喜欢，用不用得到，还有好不好用。要知道，安装的插件越多，编辑器的性能越差，越容易出现编辑器闪退。所以，尽可能少地安装插件，卸载几乎不用的插件。

**1.Chinese (Simplified) Language Pack for Visual Studio Code（VSCode中文语言包）**

![expand](./images/expand1.png)

**2.Open-In-Browser （默认浏览器打开）**

![expand](./images/expand2.png)

**3.SVG Viewer （svg图片预览）**

该插件可以预览svg图片，可以查看图片宽高，还可以生成png格式的图片。

![expand](./images/expand3.gif)

**4.background-cover（编辑器背景图片）**

插件安装后须以管理员身份运行。插件功能介绍：

- 选择和改变背景图
- 可以添加多张图片
- 可选本地和网络图片
- 可以设置图片透明度
- 可以开启和关闭自动切换

![expand](./images/expand4.gif)

**5.GitLens（Git插件）**

VSCode上下载最多的，最好用的Git插件，开发人员必备插件之一。

![expand](./images/expand5.gif)

**6.Vetur （Vue插件）**

Vue文件编译工具，是VSCode上开发Vue项目必备插件。

![expand](./images/expand6.png)

**7.Atom One Dark Theme（主题）**

个人觉得用起来眼睛比较舒适的主题插件，推荐一下。

![expand](./images/expand7.png)

**8.Auto Close Tag（自动闭合标签）**

对于标准 html 元素，这个插件没什么用。但对于非标准 html 元素，比如自定义 Vue 组件 ```<left-aside></left-aside>```，该插件会自动补全闭合标签 ```</left-aside>```。

![expand](./images/expand8.png)

**9.Auto Rename Tag（自动重命名标签）**

这是前端开发常用插件之一。无论是修改开始标签还是闭合标签，元素都会重命名。相信 VSCode 迟早会内置该项功能。

![expand](./images/expand9.png)

**10.Browser Preview**

该插件可以在编辑器右侧打开一个预览页面，实现一边编辑一边预览。多用于html文件和Vue等项目。

![expand](./images/browser_preview.gif)

不仅如此，该插件的一个隐藏功能是，它可以作为一个浏览器使用。比如你要学习新的内容并要做些笔记。

![expand](./images/browser_preview2.gif)


**11.Todo Tree**

这是一个能够做标记的插件，以方便记录待完成的事项。两个基本命令，用起来很简单：

![expand](./images/todo_tree1.png)

![expand](./images/todo_tree2.gif)

**12.Material Icon Theme**

图标主题插件，用于区分文件格式，用与不用的区别是下面这样的：

![expand](./images/Material-Icon-Theme.gif)

**13.ESlint**

团队协作，统一代码风格，自动修复eslint报错，ESlint是前端常用插件之一。

![expand](./images/eslint.png)

**14.Setting Sync**

如果还在为换了电脑还要重复设置VSCode的配置项、重新安装各种插件而烦恼的话，那你需要这个同步VSCode设置的插件 Setting Sync。

![expand](./images/Setting_Sync.png)


# 【3】常用功能

## 控制编辑器各模块的显示隐藏与位置。

- 有时你可能并不想显示缩略图或导航历史，可以通过“查看” => “显示缩略图”进行隐藏，使得编辑器页面更加简洁。

![view-module](./images/view-module.jpg)

- 可以根据个人喜好设置侧边栏或终端面板的位置，比如显示在编辑器右侧。

![module-position](./images/module-position.png)

## 工作区：多个项目在同一窗口打开。

打开多个项目的常规操作是一个项目打开一个窗口，但切换起来很费劲怎么办？这个时候“工作区”就派上用场了，它的作用就是将多个项目在同一窗口打开。

操作很简单：“文件” => “将文件夹添加到工作区”，用不着了还可以从工作区删除。  提示：Git、终端命令等都会根据项目区分开来，不会混淆。

![single-workspace](./images/single-workspace.png)

## 用户代码片段（snippet）

如果新建一个基础结构的html文件，你还要手动书写或复制粘贴吗？大可不必！VSCode 内置了代码片段功能，只需把常用的代码片段维护上去即可方便使用。

如何查看或新建“代码片段”？  请选择 “文件” => “首选项” => “用户片段”

![single-workspace](./images/code-snippet.gif)


## Emmet

Emmet 和 代码片段一样，都是为节省时间，提高效率而设计。Emmet 可能比代码片段更常用。

![single-workspace](./images/Emmet1.png)

如何不再一行行输入，而是快速生成图中的html代码？可以这样写：

```
div#box>p.title+ul>li.item${这是第$个li}*3^div#box2
```

![single-workspace](./images/Emmet2.gif)

Emmet 语法：

```
id指令 #   class指令 .   子节点 >   兄弟节点 +    ^上级节点 ^^上上级节点 以此类推

重复指令 *n，n表示重复次数。  分组指令()     属性：[attr=value]    

编号（位数）：$  $(1)，$$(01),$$$(001)      文本{}，可以与编号一起使用。
```

分组指令举例：
```
div>(ul>li>a)+div>p

    <div>
        <ul>
            <li><a href=""></a></li>
        </ul>
        <div>
            <p></p>
        </div>
    </div>

```

## 打开新的搜索编辑器

- 相比侧边栏中的搜索结果，新的编辑器页面更大，搜索结果少时更方便预览和查找，定位也更准确。
- 如果搜索结果特别多，关联很多文件时，点击 “全部折叠” 按钮后，再去查看某个文件可能更方便。

![new_search](./images/new_search.gif)

# 【4】常见问题

## 无法在根目录创建文件（夹）？

不知你是否也有这样的经历，分明点中的是根目录，但新建的文件（夹）就是不在根目录下。用了那么久vscode，我也是今天才发现怎么用，怎么实现的请看下方动图。

![cant_mkdir](./images/cant_mkdir.gif)
